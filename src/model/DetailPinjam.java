
package model;

import java.util.Date;

/**
 *
 * @author xcode
 */
public class DetailPinjam {
    private int noPinjam;
    private int noDetail;
    private int idBuku;
    private String judul;
    private Date tanggalKembali;
    private int terlambat;
    private int denda;

    public int getNoPinjam() {
        return noPinjam;
    }

    public void setNoPinjam(int noPinjam) {
        this.noPinjam = noPinjam;
    }

    public int getNoDetail() {
        return noDetail;
    }

    public void setNoDetail(int noDetail) {
        this.noDetail = noDetail;
    }

    public int getIdBuku() {
        return idBuku;
    }

    public void setIdBuku(int idBuku) {
        this.idBuku = idBuku;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Date getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(Date tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public int getTerlambat() {
        return terlambat;
    }

    public void setTerlambat(int terlambat) {
        this.terlambat = terlambat;
    }

    public int getDenda() {
        return denda;
    }

    public void setDenda(int denda) {
        this.denda = denda;
    }
    
    
}

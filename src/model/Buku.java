
package model;

/**
 *
 * @author xcode
 */
public class Buku {
    private int id;
    private String judul;
    private String Pengarang;
    private String Penerbit;
    private String Tahun;
    private String jumlah_tersedia;
    private String jumlah_terpinjam;
    private String jumlah_total;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPengarang() {
        return Pengarang;
    }

    public void setPengarang(String Pengarang) {
        this.Pengarang = Pengarang;
    }

    public String getPenerbit() {
        return Penerbit;
    }

    public void setPenerbit(String Penerbit) {
        this.Penerbit = Penerbit;
    }

    public String getTahun() {
        return Tahun;
    }

    public void setTahun(String Tahun) {
        this.Tahun = Tahun;
    }

    public String getJumlah_tersedia() {
        return jumlah_tersedia;
    }

    public void setJumlah_tersedia(String jumlah_tersedia) {
        this.jumlah_tersedia = jumlah_tersedia;
    }

    public String getJumlah_terpinjam() {
        return jumlah_terpinjam;
    }

    public void setJumlah_terpinjam(String jumlah_terpinjam) {
        this.jumlah_terpinjam = jumlah_terpinjam;
    }

    public String getJumlah_total() {
        return jumlah_total;
    }

    public void setJumlah_total(String jumlah_total) {
        this.jumlah_total = jumlah_total;
    }
    
    
}

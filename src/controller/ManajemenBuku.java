/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Buku;
import view.vBuku;

/**
 *
 * @author xcode
 */
public class ManajemenBuku {
    PreparedStatement pst;
    ResultSet rs;
    String sql;
    private vBuku book;
    
    public void tampilTabelBuku(){
        KoneksiDB.getKoneksi();
        try {
            Statement stat = KoneksiDB.getKoneksi().createStatement();
            rs = stat.executeQuery("SELECT * FROM tbbuku");
            while(rs.next()){
                
            }
        } catch (Exception e) {
        }
    }
    
    
    public void loadDataBuku(DefaultTableModel namatabel, String modf) throws SQLException{
        namatabel.getDataVector().removeAllElements();
        namatabel.fireTableDataChanged();
        
        if(modf == null){
           sql = "SELECT Id, judul, pengarang, penerbit, tahun, jumlahtersedia, jumlahterpinjam, jumlahtotal FROM tbbuku";
          // pst.setString(1, modf.toString());
           }
        
        
        pst = KoneksiDB.getKoneksi().prepareStatement(sql);
        rs = pst.executeQuery(sql);
        while(rs.next()){
            Object[] o = new Object[8];
            o[0] = rs.getString("id");
            o[1] = rs.getString("judul");
            o[2] = rs.getString("pengarang");
            o[3] = rs.getString("penerbit");
            o[4] = rs.getString("tahun");
            o[5] = rs.getString("jumlahTersedia");
            o[6] = rs.getString("jumlahTerpinjam");
            o[7] = rs.getString("jumlahTotal");
            
            namatabel.addRow(o);
        }
        rs.close();
        pst.close();
    }
    
    public void updateBuku() throws SQLException{
              
        
            KoneksiDB.getKoneksi();
            try {
                Statement stat = KoneksiDB.getKoneksi().createStatement();
                stat.executeUpdate("UPDATE tbbuku SET "
                        + "Judul='" + book.getTxtJudul().getText() + "',"
                        + "Pengarang='" + book.getTxtPengarang().getText() + "',"
                        + "Penerbit='" + book.getTxtPenerbit().getText() + "',"
                        + "Tahun='" + book.getTxtTahun().getText() + "',"
                        + "Jumlahtersedia='" + book.getTxtTersedia().getText() + "',"
                        + "Jumlahterpinjam='" + book.getTxtTerpinjam().getText() + "',"
                        + "Jumlahtotal='" + book.getTxtTotal().getText() + "'"
                        + " WHERE "
                        + "Id='" + book.getTxtId().getText() + "'"
                );
                
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,"error ManajemenBuku "+e);
            }
            
    }
    
    public void tambahBuku(Buku modp) throws SQLException {
        sql = "INSERT INTO tbbuku values(default, ?, ?, ?, ?, ?, ?, ?)";
        pst = KoneksiDB.getKoneksi().prepareStatement(sql);
        
        pst.setString(1, modp.getJudul());
        pst.setString(2, modp.getPengarang());
        pst.setString(3, modp.getPenerbit());
        pst.setString(4, modp.getTahun());
        pst.setString(5, modp.getJumlah_tersedia());
        pst.setString(6, modp.getJumlah_terpinjam());
        pst.setString(7, modp.getJumlah_total());

        pst.executeUpdate();
        pst.close();
        
    }
    
    public void hapusBuku(Buku modp) throws SQLException {
    
        sql = "DELETE FROM tbbuku WHERE id=? ";
        pst = KoneksiDB.getKoneksi().prepareStatement(sql);
        String id = String.valueOf(modp.getId());
        pst.setString(1, id);
        
        pst.executeUpdate();
        pst.close();
    
    }
}
